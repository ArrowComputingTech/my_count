#!/home/hz/.rbenv/shims/ruby

require './eech.rb'

module Enumerable
  def my_count?
    retr = []
    self.each do |x|
      if yield(x)
        retr.push(self[x])
      else
      end
    end
    return retr.length
  end
end

arr = [3,4]
puts arr.my_count? { |x| x >= 3 }
puts arr.my_count? { |x| x <= 3 }
